Login Manager
==========
Progetto Libreria Android che definisce un Fragment da utilizzare per la relizzazione di una login tramite pin password.

MinSDK = 8

Lingue disponibili
* Inglese
* Italiano

[![Alt][screenshot1_thumb]][screenshot1]    [![Alt][screenshot2_thumb]][screenshot2]

[screenshot1_thumb]: https://bitbucket.org/delbio/login-manager/raw/master/screenshots/screenshott.png
[screenshot1]: https://bitbucket.org/delbio/login-manager/raw/master/screenshots/screenshot.png
[screenshot2_thumb]: https://bitbucket.org/delbio/login-manager/raw/master/screenshots/screenshot-landt.png
[screenshot2]: https://bitbucket.org/delbio/login-manager/raw/master/screenshots/screenshot-land.png



How to use AccessLoginFrament
=============================
Il codice seguente mostra come utilizzare il fragment AccessLoginFragment:

    public class AccessLoginActivity extends SingleFragmentActivity 
    implements AccessLoginFragment.Callbacks{

        @Override
        protected Fragment createFragment() {
            return AccessLoginFragment.newInstance("com.google.android.apps.maps", "0721");
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            super.onCreate(savedInstanceState);
        }

        @Override
        public void onSuccess(String packageName) {
            Toast.makeText(this, "password corretta", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void error(Exception e) {
            Toast.makeText(this, "errore: "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void back() {
            finish();
        }

        @Override
        public void onForgotPassword() {
            Toast.makeText(this, "Recupero Password", Toast.LENGTH_SHORT).show();
        }

    }
    
