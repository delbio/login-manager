package it.delbio.login;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class AccessLoginFragment extends Fragment {

	private static final String TAG = "RemoteControlFragment";

	public static final String EXTRA_PACKAGE_NAME = "it.delbio.login.package_name";
	public static final String EXTRA_PACKAGE_PASS = "it.delbio.login.package_pass";

	public AccessLoginFragment() {}

	private EditText mPasswordEditText;
	private PackageManager pm;
	private String mPackageName;
	private String mCorrectPassword;
	private String mTempPassword;
	private Callbacks mCallbacks;
	private boolean isHandleForgotPassordEnabled;

	public static AccessLoginFragment newInstance(String packageName, String password){
		Bundle args = new Bundle();
		args.putString(EXTRA_PACKAGE_NAME, packageName);
		args.putString(EXTRA_PACKAGE_PASS, password);

		AccessLoginFragment fragment = new AccessLoginFragment();
		fragment.setArguments(args);

		return fragment;
	}

	/**
	 * Required interface to hosting activities
	 */
	public interface Callbacks{

		void onSuccess(String packageName);
		void error(Exception e);
		void onForgotPassword();
		void back();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		pm = getActivity().getPackageManager();
		mPackageName = getArguments().getString(EXTRA_PACKAGE_NAME);
		mCorrectPassword = getArguments().getString(EXTRA_PACKAGE_PASS);
		mTempPassword = "";
		setRetainInstance(true);
		isHandleForgotPassordEnabled = true;
	}

	private void handleForgotPassword(final EditText mEditText){
		Drawable err_indiactor = getResources().getDrawable(android.R.drawable.ic_dialog_alert);
		mEditText.setCompoundDrawablesWithIntrinsicBounds(null, null, err_indiactor, null);
		// http://stackoverflow.com/questions/3554377/handling-click-events-on-a-drawable-within-an-edittext
		mEditText.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				final int DRAWABLE_LEFT = 0;
				final int DRAWABLE_TOP = 1;
				final int DRAWABLE_RIGHT = 2;
				final int DRAWABLE_BOTTOM = 3;

				if(event.getAction() == MotionEvent.ACTION_UP) {
					if(event.getX() >= (mEditText.getRight() - mEditText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
						mCallbacks.onForgotPassword();
					}
				}
				return false;
			}
		});
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_access_login, container, false);

		View appDataContainerView = v.findViewById(R.id.app_data_container);

		mPasswordEditText = (EditText) appDataContainerView
				.findViewById(R.id.app_passEditText);

		if (isHandleForgotPassordEnabled) {
			handleForgotPassword(mPasswordEditText);
		}
		

		// Hide the keyboard. for the mPasswordEditText
		mPasswordEditText.setInputType(EditorInfo.TYPE_NULL);

		View appInfoView = appDataContainerView.findViewById(R.id.app_info);

		TextView appNameTextView = (TextView) appInfoView
				.findViewById(R.id.app_nameTextView);

		ImageView mAppIconImageView = (ImageView) appInfoView
				.findViewById(R.id.app_iconImageView);

		try {
			ApplicationInfo appInfo = pm.getApplicationInfo(mPackageName, 0);
			appNameTextView.setText(appInfo.loadLabel(pm));
			mAppIconImageView.setImageDrawable(appInfo.loadIcon(pm));
		} catch (NameNotFoundException e) {
			if (BuildConfig.DEBUG) Log.e(TAG, "error: ",e);
			mCallbacks.error(e);
		}

		View.OnClickListener numberButtonListener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				TextView textView = (TextView) v;

				String working = mPasswordEditText.getText().toString();
				mPasswordEditText.setText( working + "*" );

				String text = textView.getText().toString();
				mTempPassword = mTempPassword + text;

				if (mTempPassword.equals(mCorrectPassword)){
					mCallbacks.onSuccess(mPackageName);
				}
			}
		};

		TableLayout tableLayout = (TableLayout) v
				.findViewById(R.id.pin_pad_tableLayout);
		int number = 1;
		for(int i = 0; i < tableLayout.getChildCount() -1; i++){
			TableRow row = (TableRow) tableLayout.getChildAt(i);
			for (int j = 0; j < row.getChildCount(); j++) {
				Button button = (Button) row.getChildAt(j);
				button.setText(""+number);
				button.setOnClickListener(numberButtonListener);
				number++;
			}
		}

		TableRow bottomRow = (TableRow) tableLayout
				.getChildAt(tableLayout.getChildCount() -1);
		Button deleteButton = (Button) bottomRow.getChildAt(0);
		deleteButton.setText(R.string.access_login_button_back);
		deleteButton.setTextAppearance(getActivity(), R.style.RemoteButton_Border);
		deleteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mCallbacks.back();
			}
		});

		Button zeroButton = (Button) bottomRow.getChildAt(1);
		zeroButton.setText("0");
		zeroButton.setOnClickListener(numberButtonListener);

		Button enterButton = (Button) bottomRow.getChildAt(2);
		enterButton.setText("<-");
		enterButton.setTextAppearance(getActivity(), R.style.RemoteButton_Border);
		enterButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				CharSequence working = mPasswordEditText.getText();
				if (working.length() > 0){
					mPasswordEditText.setText(working.subSequence(0, working.length() -1));
					mTempPassword = mTempPassword.substring(0, mTempPassword.length() -1);
				}

			}
		});
		enterButton.setOnLongClickListener(new View.OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				mTempPassword = "";
				mPasswordEditText.setText("");
				return true;
			}
		});

		return v;
	}

}
