package it.delbio.login;

import it.delbio.login.AccessLoginFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Window;
import android.widget.Toast;

public class AccessLoginActivity extends SingleFragmentActivity 
implements AccessLoginFragment.Callbacks{

	@Override
	protected Fragment createFragment() {
		return AccessLoginFragment.newInstance("com.google.android.apps.maps", "0721");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onSuccess(String packageName) {
		Toast.makeText(this, "password corretta", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void error(Exception e) {
		Toast.makeText(this, "errore: "+e.getMessage(), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void back() {
		finish();
	}

	@Override
	public void onForgotPassword() {
		Toast.makeText(this, "Recupero Password", Toast.LENGTH_SHORT).show();
	}

}
